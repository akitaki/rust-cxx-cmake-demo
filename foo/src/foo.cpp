#include "foo.hpp"
#include <iostream>

#include <torch/torch.h>

Foo::Foo() {
    torch::Tensor tensor = torch::eye(3);
    std::cout << tensor << std::endl;
    std::cout << "Foo created\n";
}
